<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Products Page</title>
</head>
<body>
	<div>
<div class="container" style="margin:50px">
    <div class="row text-center"><strong> Product Details</strong></div>
    <div class="row" style="border:1px solid green;padding:10px">
        <div class="col-md-3 text-center"><strong>Product Name</strong></div>
        <div class="col-md-3 text-center"><strong>Brand</strong></div>
        <div class="col-md-3 text-center"><strong>Price</strong></div>
    </div>
    <c:forEach items="${allProducts}" var="prod">
        <div class="row" style="border:1px solid green;padding:10px">
         <div class="col-md-3 text-center">${prod.product_name}</div>
         <div class="col-md-3 text-center" >${prod.product_brand}</div>
         <div class="col-md-3 text-center">${prod.product_price}</div>
        </div>
    </c:forEach>

</div>
</div>
</body>
</html>
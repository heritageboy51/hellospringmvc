package com.chandan.services;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.chandan.dao.ProductsDao;
import com.chandan.dao.ProductsDaoImplement;

@Service
public class ProductDetailsImpl implements ProductDetails {
	
	@Autowired
	ProductsDaoImplement productDao;

	@Override
	public List getProductDetails() {
		
		return productDao.getAllProducts();
		
		/*
		 * Products p1 = new Products(); p1.setProductName("Television");
		 * p1.setProductBrand("Sony"); p1.setProductPrice("Rs. 49,990");
		 * 
		 * Products p2 = new Products(); p2.setProductName("Washing Machine");
		 * p2.setProductBrand("Bosch"); p2.setProductPrice("Rs. 44,990");
		 * 
		 * Products p3 = new Products(); p3.setProductName("Refregerator");
		 * p3.setProductBrand("Samsung"); p3.setProductPrice("Rs. 55,990");
		 * 
		 * Products p4 = new Products(); p4.setProductName("Air Conditioner");
		 * p4.setProductBrand("Hitachi"); p4.setProductPrice("Rs. 59,990");
		 * 
		 * return Arrays.asList(p1, p2, p3, p4);
		 */
	}

}

package com.chandan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.chandan.services.ProductDetailsImpl;



@Controller
public class ProductController {

	
	@Autowired
	private ProductDetailsImpl productDetails;// = new ProductDetailsImpl();
	
	@RequestMapping(value="/products", method=RequestMethod.GET)
	public ModelAndView products() {
		
		List allProductsDetails = productDetails.getProductDetails();
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("allProducts", allProductsDetails);
		mv.setViewName("products");
		
		return mv;
	}

}

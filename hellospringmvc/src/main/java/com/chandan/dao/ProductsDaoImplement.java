package com.chandan.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.chandan.model.my_products;
import com.chandan.rowmapper.ProductRowMapper;

@Component
public class ProductsDaoImplement implements ProductsDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	HibernateTemplate hibernateTemplate;
	
	@Autowired
	SessionFactory SessionFactory;

	@SuppressWarnings("unchecked")
	@Transactional
	public List getAllProducts() {
		//jdbc raw query way of fetching data. No Hibernate
		//List<Products> allProducts = (List<Products>) jdbcTemplate.query("select * from my_products", new ProductRowMapper());
		
		//using Hibernet ORM now
		//List<my_products> allProducts = (List<my_products>) hibernateTemplate.loadAll(my_products.class);
		
		Session session = SessionFactory.openSession();
		List<my_products> allProducts = (List<my_products>) session.createQuery("From my_products").list();
		session.close();
		
	
		return allProducts;
	}

}

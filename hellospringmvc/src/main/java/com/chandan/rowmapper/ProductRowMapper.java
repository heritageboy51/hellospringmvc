package com.chandan.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chandan.model.my_products;

public class ProductRowMapper implements RowMapper<my_products> {

	@Override
	public my_products  mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		my_products p = new my_products();
		p.setProduct_name(rs.getString("product_name"));
		p.setProduct_brand(rs.getString("product_brand"));
		p.setProduct_price(rs.getString("product_price"));
		
		return p;
	}

}
